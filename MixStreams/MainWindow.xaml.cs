﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using NAudio.Wave;
using NAudio.CoreAudioApi;
using EkAudioLib.Core;
using System.IO;

namespace MixStreams
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private MMDevice selectedDevice;
        public IEnumerable<MMDevice> CaptureDevices { get; private set; }
        WasapiCapture capture = null;
        WaveFormat _waveFormat = new WaveFormat(44100, 24, 2);
        WaveFileWriter writer = null;
        WaveFileReader reader = null;
        WaveOut waveout = new WaveOut();
        
        
        public MainWindow()
        {
            InitializeComponent();
            
        }



        private void Record_Click(object sender, RoutedEventArgs e)
        {

            //Select capture device 
            var enumerator = new MMDeviceEnumerator();
            CaptureDevices = enumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active).ToArray();
            var defaultDevice = enumerator.GetDefaultAudioEndpoint(DataFlow.Capture, Role.Console);
            selectedDevice = CaptureDevices.FirstOrDefault(c => c.ID == defaultDevice.ID);

            Record();

        }

        private void Record()
        {
            try
            {
               
                writer = new WaveFileWriter("record.wav", _waveFormat);

                capture = new WasapiCapture(selectedDevice);
                capture.ShareMode = AudioClientShareMode.Exclusive;
                capture.WaveFormat = _waveFormat;
                capture.DataAvailable += CaptureOnDataAvailable;
                capture.RecordingStopped += capture_RecordingStopped;
                capture.StartRecording();
                Console.WriteLine("Recording....");
                ;

            }
            catch (Exception e)
            {
                MessageBox.Show("Record: " + e.Message);


            }
        }

     
        
        void capture_RecordingStopped(object sender, StoppedEventArgs e)
        {
            

        }

        private void CaptureOnDataAvailable(object sender, WaveInEventArgs waveInEventArgs)
        {
            try
            {
                writer.Write(waveInEventArgs.Buffer, 0, waveInEventArgs.BytesRecorded);
                writer.Flush();
               
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {


            if (capture != null)
            {
                capture.StopRecording();
                capture.Dispose();
                capture = null;
                Console.WriteLine("Recording stopped...");
            }

            if (writer != null)
            {
                writer.Close();
                //writer.Dispose();
                Console.WriteLine("Writer disposed");
            }
            
            reader = new WaveFileReader("record.wav");
            waveout.Init(reader);
            waveout.Play();
            waveout.PlaybackStopped += o_PlaybackStopped;


        }

        void o_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            reader.Dispose();
            Console.WriteLine("reader disposed");
        }
    }
}
