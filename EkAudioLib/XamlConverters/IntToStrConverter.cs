﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace EkLooper.XamlConverters
{
    class IntToStrConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString();   
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int i=0;
            if (value is string)
            {
                if (int.TryParse(value.ToString(), out i))
                {
                    return i;
                }
            }
            
            return i;
            
        }

        #endregion
    }
}
