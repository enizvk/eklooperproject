﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EkLooperControls.gui
{
    public class WaveformPlayer : EkLooperLib.IWaveformPlayer
    {

        private WaveStream _source;
        public WaveformPlayer(WaveStream src)
        {
            _source = src;
        }
        
        #region IWaveformPlayer Members

        /// <summary>
        /// Gets or sets the current sound streams playback position.
        /// </summary>
        public double ChannelPosition
        {
            get
            {
                return _source.Position;
                
            }
            set
            {
                _source.Position = (long)value;
            }
        }

        /// <summary>
        /// Gets the total channel length in seconds.
        /// </summary>
        public double ChannelLength
        {

            get
            {
                return _source.TotalTime.Seconds;
                //throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the raw level data for the waveform.
        /// </summary>
        /// <remarks>
        /// Level data should be structured in an array where each sucessive index
        /// alternates between left or right channel data, starting with left. Index 0
        /// should be the first left level, index 1 should be the first right level, index
        /// 2 should be the second left level, etc.
        /// </remarks>
        public float[] WaveformData
        {
            get
            {
                
                
                throw new NotImplementedException();
                

            }
        }
        /// <summary>
        /// Gets or sets the starting time for a section of repeat/looped audio.
        /// </summary>
        public TimeSpan SelectionBegin
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        /// <summary>
        /// Gets or sets the ending time for a section of repeat/looped audio.
        /// </summary>
        public TimeSpan SelectionEnd
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region ISoundPlayer Members

        public bool IsPlaying
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

}
