﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using EkAudioLib.Core;
using System.Windows.Input;

namespace EkAudioLib.Gui
{

    [DisplayName("Looper Line")]
    [Description("Individual looping line")]
    [ToolboxItem(true)]
    [TemplatePart(Name = "PART_BtnPlay", Type = typeof(Button)),
     TemplatePart(Name = "PART_BtnRec", Type = typeof(Button)),
     TemplatePart(Name = "PART_Waveform", Type = typeof(WaveformTimeline)),
     TemplatePart(Name = "PART_chkLoop", Type = typeof(CheckBox)),
     TemplatePart(Name = "PART_Number", Type = typeof(Label)),
     TemplatePart(Name = "PART_SliderVolume", Type = typeof(Slider))]
    public class EkLoopLine : Control
    {

        #region Fields
        const string IDE_LoopControls = "LoopLineControls";

        private Button _btnPlay;
        private Button _btnRec;
        private Slider _sldrVolume;
        private Label _lbNumber;
        private CheckBox _chkIsLooping;
        private WaveformTimeline _Waveform;
        #endregion

        #region SooundPlayer
        private IWaveformPlayer _soundPlayer = null;
        public IWaveformPlayer SoundPlayer
        {
            get { return _soundPlayer; }
            set
            {
                if (_soundPlayer != value)
                    _soundPlayer = value;
                    _Waveform.RegisterSoundPlayer(value);
            }
        }
        #endregion

        #region Dependancy Properties
        
        #region PanelBackgroundProperty
        [Category(IDE_LoopControls)]
        public Brush PanelBackground
        {
            get { return (Brush)GetValue(PanelBackgroundProperty); }
            set { SetValue(PanelBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PanelBackground.  This enables animation, styling, binding, etc...

        public static readonly DependencyProperty PanelBackgroundProperty =
            DependencyProperty.Register("PanelBackground", typeof(Brush), typeof(EkLoopLine),
            new PropertyMetadata(new SolidColorBrush(Colors.Gray)));
        #endregion

        #region PlayButtonText
        [Category(IDE_LoopControls)]
        public string PlayButtonText
        {
            get { return (string)GetValue(PlayButtonTextProperty); }
            set { SetValue(PlayButtonTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PlayButtonText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlayButtonTextProperty =
            DependencyProperty.Register("PlayButtonText", typeof(string), typeof(EkLoopLine), new PropertyMetadata("Play"));
        #endregion

        #region RecButtonText
        [Category(IDE_LoopControls)]
        public string RecButtonText
        {
            get { return (string)GetValue(RecButtonTextProperty); }
            set { SetValue(RecButtonTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RecButtonText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RecButtonTextProperty =
            DependencyProperty.Register("RecButtonText", typeof(string), typeof(EkLoopLine), new PropertyMetadata("Record"));
        #endregion

        #region LoopCheckBoxText
        [Category(IDE_LoopControls)]
        public string LoopCheckBoxText
        {
            get { return (string)GetValue(LoopCheckBoxTextProperty); }
            set { SetValue(LoopCheckBoxTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for loopCheckBoxText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LoopCheckBoxTextProperty =
            DependencyProperty.Register("LoopCheckBoxText", typeof(string), typeof(EkLoopLine), new PropertyMetadata("Loop"));
        #endregion

        #region IsLoopChecked
        [Category(IDE_LoopControls)]
        public bool IsLoopChecked
        {
            get { return (bool)GetValue(IsLoopCheckedProperty); }
            set { SetValue(IsLoopCheckedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsLoopChecked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsLoopCheckedProperty =
            DependencyProperty.Register("IsLoopChecked", typeof(bool), typeof(EkLoopLine), new PropertyMetadata(true));

        #endregion

        #region WaveformBackground
        [Category(IDE_LoopControls)]
        public Brush WaveformBackground
        {
            get { return (Brush)GetValue(WaveformBackgroundProperty); }
            set { SetValue(WaveformBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WaveformBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WaveformBackgroundProperty =
            DependencyProperty.Register("WaveformBackground", typeof(Brush), typeof(EkLoopLine),
            new PropertyMetadata(new SolidColorBrush(Colors.Aqua)));
        #endregion

        #region IsSelected
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(EkLoopLine), new PropertyMetadata(false));

        #endregion

        #region Number
        public int Number
        {
            get { return (int)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Number.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberProperty =
            DependencyProperty.Register("Number", typeof(int), typeof(EkLoopLine), new PropertyMetadata(0));

        #endregion

        #endregion  Dependancy Properties

        #region Events
        public event RoutedEventHandler LoopCheckClicked;
        public event RoutedEventHandler PlayButtonClicked;
        public event RoutedEventHandler RecordButtonClicked;
        #endregion

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _Waveform = GetTemplateChild("PART_Waveform") as WaveformTimeline;

            //Get instances from xaml
            _btnPlay = GetTemplateChild("PART_BtnPlay") as Button;
            

            _btnRec = GetTemplateChild("PART_BtnRec") as Button;
            // _btnRec.Click += btnRec_Click;

            _sldrVolume = GetTemplateChild("PART_SliderVolume") as Slider;
            //_sldrVolume.ValueChanged += sldrVolume_ValueChanged;

            _chkIsLooping = GetTemplateChild("PART_chkLoop") as CheckBox;

            _lbNumber = GetTemplateChild("PART_Number") as Label;

            _chkIsLooping.Click += new RoutedEventHandler((o,a)=>
            {
                if (LoopCheckClicked != null)
                    LoopCheckClicked(o, a);
            });

            _btnPlay.Click += new RoutedEventHandler((o, a) => 
                {
                    if (PlayButtonClicked != null) 
                        PlayButtonClicked(o, a);
                });

            _btnRec.Click += new RoutedEventHandler((o, a) =>
            {
                if (RecordButtonClicked != null)
                    RecordButtonClicked(o, a);
            });




            //When user clicks to buttons select current
            _lbNumber.MouseDown += (o, a) =>
            {

                IsSelected = !IsSelected;
            };
        }      

        static EkLoopLine()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EkLoopLine),
                new FrameworkPropertyMetadata(typeof(EkLoopLine)));
        }

        public EkLoopLine()
        {

        }
    }
}
