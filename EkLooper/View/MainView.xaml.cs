﻿using System.Windows;
using EkAudioLib.Core;

namespace EkLooper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {

        public readonly App AppInstance;
        SoundPlayer engine = new SoundPlayer(); 

        public MainWindow()
        {
            AppInstance = (App)App.Current;
            InitializeComponent();

         

           // engine.OpenFile(@"C:\Users\User11\Desktop\audio1.mp3");
         //   engine.Play();
            
            
        }

        private void ShowSettings_Click(object sender, RoutedEventArgs e)
        {
//LoopLine1.PlaybackEngine = null;

            if (AppInstance != null) 
                AppInstance.ShowSettingsWindow();
        }

    }
}
