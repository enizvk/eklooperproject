﻿using System;
using System.ComponentModel;

namespace EkLooper.ViewModel
{
    public abstract class ViewModel : INotifyPropertyChanged, IDisposable
    {


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string PropertyName)
        {

            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                PropertyChangedEventArgs args = new PropertyChangedEventArgs(PropertyName);
                handler(this, args);
            }


        }

        #region IDisposable Members
        // Flag: Has Dispose already been called? 
        bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers. 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here. 
                //
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
        }

        #if DEBUG
        ~ViewModel()
        {
            string msg = string.Format("{0} ({1}) Finalized", this.GetType().Name, this.GetHashCode());
            System.Diagnostics.Debug.WriteLine(msg);
            Dispose(false);
        }
        #endif


        #endregion
    }
}
