﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using NAudio.CoreAudioApi;

namespace EkLooper.ViewModel
{


    internal class SettingsViewModel : ViewModel
    {

        #region Properties

        #region CurrentUI
        public UserControl CurrentUI
        {
            get
            {
                if (SelModule == null) { return null; }
                else { return this.SelModule.UI; }
            }
        }
        #endregion

        #region SelModule
        private IModule _selModule;
        public IModule SelModule
        {
            get { return _selModule; }
            set
            {
                if (value != _selModule)
                {
                    if (_selModule != null)
                    {
                        _selModule.Destroy();
                    }
                }

                _selModule = value;
                RaisePropertyChanged("SelModule");
                RaisePropertyChanged("CurrentUI");
            }
        }
        #endregion

        public List<IModule> Modules { get; private set; }
        #endregion

        #region Public Methods

        public SettingsViewModel()
        {

        }

        public SettingsViewModel(IEnumerable<IModule> modules)
        {
            this.Modules = modules.
                Where(m => m.isAvilable == true).
                OrderBy(m => m.Order).ToList();

            if (this.Modules.Count > 0)
            {
                //Select first module by default
                _selModule = Modules[0];
            }
        }

        public void SaveSettings()
        {
            //save
        }
        #endregion

        #region Private Methods

        private void UpdateRecordingDevices()
        {
            //RecDevices.Clear();
            //var e = new MMDeviceEnumerator();
            //var devs = e.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active).ToList();
            //foreach (var item in devs)
            //{
            //    RecDevices.Add(item);
            //}
        }

        private MMDevice FindMMDevice(string FriendlyName, IEnumerable<MMDevice> devs)
        {
            var found = devs.Where(d => d.FriendlyName == FriendlyName).FirstOrDefault();
            return found;
        }
        #endregion

    }
}