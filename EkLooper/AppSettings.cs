﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EkLooper
{
    [Serializable]
     class AppSettings : ApplicationSettingsBase
    {
        [UserScopedSetting()]        
        public string SelRecDevice
        {
            get { return ((string)this["SelRecDevice"]); }
            set { this["SelRecDevice"] = (string)value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("")]
        public string SelAudioHost
        {
            get { return (string)(this["SelAudioHost"]); }
            set { this["SelAudioHost"] = value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("")]
        public string SelPlaybackDev
        {
            get { return (string)(this["SelPlaybackDev"]); }
            set { this["SelPlaybackDev"] = value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("2")]
        public int SelChannelCount
        {
            get { return (int)(this["SelChannelCount"]); }
            set { this["SelChannelCount"] = value; }
        }

    
    }
}
