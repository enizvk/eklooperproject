﻿using System.Windows;
using System.ComponentModel.Composition.Hosting;
using System;


namespace EkLooper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private View.SettingsView _settingsView;

        private void App_Startup(object sender, StartupEventArgs e)
        {
                    
            /*================= Init Main Window =================*/
            var mainView = new MainWindow();
            var mainVm = new ViewModel.MainViewViewModel();
            mainView.DataContext = mainVm;
            mainView.Closing += (s, args) => { Shutdown(); };

            
            /*================= Init Settings Window =================*/
            _settingsView = new View.SettingsView();

            // Microsoft MEF framework
            var catalog = new AssemblyCatalog(this.GetType().Assembly);
            var container = new CompositionContainer(catalog);

            //Get the base module for the plugins
            var modules = container.GetExportedValues<IModule>();

            var settVm = new ViewModel.SettingsViewModel(modules);
            _settingsView.DataContext = settVm;            
            _settingsView.Closing += (s, args) => 
                {
                    if (settVm.SelModule != null) settVm.SelModule.Destroy();
                    
                    //Provide reusing without new instances  
                    args.Cancel = true;
                    _settingsView.Visibility = Visibility.Hidden;
                };

            mainView.Show();            

            
        }

        public void ShowSettingsWindow()
        {
            if(_settingsView!=null) 
                _settingsView.ShowDialog();
            else { throw new ArgumentNullException("Cannot show settings window due to system error!"); }
        }

    }
   
}
