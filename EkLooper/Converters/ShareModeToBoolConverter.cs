﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using NAudio.CoreAudioApi;

namespace EkLooper.Converters
{
    class ShareModeToBoolConverter : IValueConverter
    {


        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is AudioClientShareMode)
            {
                var v = (AudioClientShareMode)value;
                if (v == AudioClientShareMode.Exclusive) return true;
            }

            return false;

        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value is bool)
            {
                if ((bool)value) return AudioClientShareMode.Exclusive;
               
            }

            return  AudioClientShareMode.Shared;
        }

        #endregion
    }
}
