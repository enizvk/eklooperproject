﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using NAudio.Wave;

namespace EkLooper.OutputDriverModules
{
    [Export(typeof(IModule))]
    internal class AsioModule : IModule
    {
        private AsioModuleViewModel _viewModel;
        private AsioModuleView _view;
        private AsioOut _asioOut;

        [ImportingConstructor]
        public AsioModule()
        {
        }

        public IWavePlayer CreateDevcie()
        {
            if (_asioOut == null) _asioOut = new AsioOut(_viewModel.SelDriver);

            return _asioOut;
        }

        private void CreateView_ViewModel()
        {
            _view = new AsioModuleView();
            _viewModel = new AsioModuleViewModel();
            _view.DataContext = _viewModel;
        }

        #region IModule Members

        public UserControl UI
        {
            get
            {
                if (_view == null) CreateView_ViewModel();
                return _view;
            }
        }

        public string Name
        {
            get { return "ASIO Driver"; }
        }

        public void Destroy()
        {
            if (_asioOut != null)
            {
                _asioOut.Stop();
                _asioOut.Dispose();
                _asioOut = null;
            }

            _viewModel.Dispose();
            _view = null;
        }

        public bool isAvilable
        {
            get { return NAudio.Wave.AsioOut.isSupported(); }
        }

        public int Order
        {
            get { return 1; }
        }

        #endregion IModule Members
    }
}