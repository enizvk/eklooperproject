﻿using System.Collections.Generic;
using System.Linq;
using NAudio.CoreAudioApi;
using NAudio.Wave;

namespace EkLooper.OutputDriverModules
{
    class WasapiModuleViewModel : ViewModel.ViewModel
    {

        #region Fields

        #endregion

        #region Properties

        #region AvilDevices
        private IEnumerable<MMDevice> _avilDevices;
        public IEnumerable<MMDevice> AvilDevices
        {
            get
            {
                UpdateAvilDevices();
                return _avilDevices;
            }
            set
            {
                if (value != _avilDevices)
                {
                    _avilDevices = value;
                    RaisePropertyChanged("AvilDevices");
                }
            }
        }
        #endregion

        #region ShareMode
        private AudioClientShareMode _shareMode ;
        public AudioClientShareMode ShareMode
        {
            get { return _shareMode; }
            set { _shareMode = value; }
        }
        
        #endregion
        
        #region SelDevice
        private MMDevice _selDevice;
        public MMDevice SelDevice
        {
            get { return _selDevice; }
            set
            {
                _selDevice = value;
                RaisePropertyChanged("SelDevice");
            }
        }
        #endregion

        public int BitRate { get; set; }
        public int Bits { get; set; }
        public int Channels { get; set; }

        private List<WaveFormat> _waveFormats = new List<WaveFormat>();
        public List<WaveFormat> WaveFormats
        {
            get { return _waveFormats; }
            set
            {
                if (value != _waveFormats)
                {
                    _waveFormats = value;
                    RaisePropertyChanged("WaveFormats");
                }

            }
        }
        #region SelectedWaveFormat

        private WaveFormat _SelectedWaveFormat = new WaveFormat();
        public WaveFormat SelectedWaveFormat
        {
            get { return _SelectedWaveFormat; }
            set
            {
                _SelectedWaveFormat = value;
                RaisePropertyChanged("SelectedWaveFormat");
                System.Console.WriteLine(_SelectedWaveFormat.ToString());
            }
        }
        #endregion


        #endregion

        public WasapiModuleViewModel()
        {

            WaveFormats.Add(new WaveFormat(44100, 16, 2));
            WaveFormats.Add(new WaveFormat(48000, 16, 2));
            WaveFormats.Add(new WaveFormat(96000, 16, 2));
            WaveFormats.Add(new WaveFormat(192000, 16, 2));
        }

        private void UpdateAvilDevices()
        {
            var enumerator = new MMDeviceEnumerator();
            _avilDevices = enumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active).ToArray();
            var defaultDevice = enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);
            SelDevice = _avilDevices.FirstOrDefault(c => c.ID == defaultDevice.ID);
            //enumerator.
        }
    }
}