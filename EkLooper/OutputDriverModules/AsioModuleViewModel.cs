﻿using System.Collections.Generic;
using System.Linq;
using NAudio.Wave;


namespace EkLooper.OutputDriverModules
{
    class AsioModuleViewModel : ViewModel.ViewModel
    {

        public RelayCommand ShowAsioSettCommand { get; private set; }

        public List<string> DriverList { get; set; }
        public string SelDriver { get; set; }

        public AsioModuleViewModel()
        {
            DriverList = AsioOut.GetDriverNames().ToList();
            if (DriverList.Count > 0) SelDriver = DriverList.First();
            ShowAsioSettCommand = new RelayCommand(ShowControlPanel);
        }

        public void ShowControlPanel()
        {
            if (SelDriver != null)
            {
                using (var a = new AsioOut(SelDriver))
                {
                    a.ShowControlPanel();
                }
            }
        }
    }
}
