﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;

namespace EkLooper.OutputDriverModules
{
    [Export(typeof(IModule))]
    internal class WasapiModule : IModule
    {
        #region IModule Members

        private WasapiOut _wasapiOut ;
        private WasapiModuleViewModel _vm;
        private WasapiModuleView _view;

        private void CreateView_ViewModel(){

            _view = new WasapiModuleView();
            _vm = new WasapiModuleViewModel();
            _view.DataContext = _vm;

        }


        [ImportingConstructor]
        public WasapiModule()
        {

        }

        public IWavePlayer CreateDevcie()
        {
            if (_wasapiOut == null) {

                _wasapiOut = new WasapiOut(NAudio.CoreAudioApi.AudioClientShareMode.Exclusive,200);
            }
            return _wasapiOut;
        }

        public System.Windows.Controls.UserControl UI
        {
            get
            {
                if (_view == null) CreateView_ViewModel();
                return _view;
            }
        }

        public bool isAvilable
        {
            // supported on Vista+
            get { return Environment.OSVersion.Version.Major >= 6; }
        }

        public string Name
        {
            get { return "WASAPI Driver"; }
        }

        public int Order
        {
            get { return 2; }
        }

        public void Destroy()
        {
            if (_wasapiOut != null)
            {
                _wasapiOut.Stop();
                _wasapiOut.Dispose();
            }

            _vm.Dispose();
            _view = null;
        }

        #endregion
    }
}
