﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EkLooper
{
    public interface IModule
    {
        UserControl UI{get;}
        bool isAvilable { get; }
        string Name{get;}
        int Order { get; }
        void Destroy();

    }
}
